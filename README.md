# How to connect my raspberry pi or beaglebone black to my home wireless?
This program allows easy wifi setup with no need for a keyboard, screen or linux knowladge. Just a your cellular or your laptop.

## How does this work?
This program will run a hotspot, which can connect to from your phone, or laptop.
The wireless hotspot will have the name cwvw prefix. After cnnecting, open a browser and surf to http://anyname/
 you will get a welcome page asking for your wireless network name and your password to it.
 Click connect and it will try to connect to your network.
 ## Can I change the wireless settings?
 The hostspot remain active so you can change the connection details, and connect to a new network. This requires an atheros based wireless device.
 ## How many wireless networks can I connect to?
 Only one active network is allowed, so a connection to an old network will be overwritten with new one.

# Requirments
## Hardware
1. Use a usb wireless adapter which supports virtual devices as the ath9k_htc. Here is a [list](https://wireless.wiki.kernel.org/en/users/drivers/ath9k_htc/devices) of such products. I used the [TL-WN722N](http://www.tp-link.com/en/products/details/TL-WN722N.html). 
2. I wrote this for my [beaglebone black](https://beagleboard.org/black), but this may also work on a raspberry pi
## Software
1. Use Debian 8 jessie. In case you use the beaglebone see [this](https://eewiki.net/display/linuxonarm/BeagleBone+Black) by RobertCNelson.

# Installing
1. Download the [deb file](https://gitlab.com/nivwiz/cwvw/raw/master/deb/cwvw_0.2-1_all.deb).
2. Install it

### Notes:
- How to reduce tx power?</br>
sudo iw phy0 set txpower fixed 100

