from setuptools import setup, find_packages
import functools
import os
import platform
import json

_PYTHON_VERSION = platform.python_version()
_in_same_dir = functools.partial(os.path.join, os.path.dirname(__file__))

project_name='cwvw'
data_dir=( 'templates', 'static' )
package_data=[]
for directory in data_dir:
  for root, dirs, files in os.walk( os.path.join(project_name, directory )):
    for f in files:
      if f.startswith('.') :
        files.remove(f)
    if len(files) and '/.' not in root:
      temp_list=root.split('/')
      temp_list.remove(project_name)
      path='/'.join( temp_list )
      package_data.extend( [ os.path.join( path, f) for f in files] )
"""
with open('packages.json', 'w') as f:
  json.dump( find_packages(), f )

with open('package_data', 'w') as f:
  for s in package_data:
        f.write(s + '\n')
"""
with open(_in_same_dir(project_name, "__version__.py")) as version_file:
        exec(version_file.read()) # pylint: disable=W0122

setup(
     name=project_name,
      classifiers=[
                    'Programming Language :: Python :: 2.7',
                    'Programming Language :: Python :: 3.4',
                ],
      description="Website to allow wifi client configuration",
      license="GPL",
      author="Niv Gal Waizer",
      author_email="nivwiz@gmail.com",
      version=__version__, # pylint: disable=E0602
      packages=find_packages( exclude=['tests'] ),
      package_data={project_name : package_data},
      include_package_data=True,
      install_requires=[ 'flask', 'flask_wtf'],
      entry_points={
        'console_scripts': [
          'cwvw = cwvw:run',
          'start_client = start_client:run',
        ],
      },
    )
