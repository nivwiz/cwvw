try:
  from flask_wtf import Form
except ImportError:
  from flask.ext.wtf import Form
from wtforms import TextField, SubmitField, IntegerField, PasswordField, FloatField, validators, ValidationError, BooleanField, SelectField

class SetupForm(Form):
  ssid = SelectField("Wireless name", choices=[])
  #ssid = TextField("Wireless name")
  password = PasswordField("Wireless Password")
  submit = SubmitField("Connect")
