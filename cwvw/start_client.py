#!/usr/bin/env python2.7
import argparse
import subprocess
import os

import gobject

import dbus
import dbus.service
import dbus.mainloop.glib

import sys
import time
from syslog import syslog

def stop_hostapd():
  syslog( 'Stop hostapd service' )
  subprocess.call(["systemctl", "stop", "hostapd"])
  subprocess.call(["/sbin/iw", "wlan0_ap", "del"])

def connect_new_wifi( ssid, password ):
  wlan0_info = subprocess.check_output(["/sbin/iw", "wlan0", "info"])
  for line in wlan0_info.split('\n'):
    if 'addr' in line:
      wlan0_mac = line.split()[-1]

  syslog( 'Disconnect previous wifi connection' )
  bus = dbus.SystemBus()
  manager = dbus.Interface(bus.get_object("net.connman", "/"),
                        "net.connman.Manager")
  path = "/net/connman/technology/wifi"
  technology = dbus.Interface(bus.get_object("net.connman", path),"net.connman.Technology")
  if not technology.GetProperties()['Powered']:
    technology.SetProperty("Powered", True)
  technology.Scan()

  for i in range(5):
    wifi_scan_list = manager.GetServices()
    if len(wifi_scan_list) > 0 :
      break

  for path, properties in manager.GetServices():
    identifier = path[path.rfind("/") + 1:]
    if properties["Type"] != 'ethernet' and properties["State"] == 'online':
      syslog( 'Disconnecting {}'.format(properties['Name']) )
      path = '/net/connman/service/' + identifier
      service = dbus.Interface(bus.get_object("net.connman", path),
                                              "net.connman.Service")
      #try:
      service.Disconnect()
      service.Remove()
      #except dbus.DBusException, error:
      #  print '{}: {}'.format(error._dbus_error_name, error.message)

  for path, properties in wifi_scan_list:
    syslog( 'Checking if this ssid: {}, is the one to connect to: {}'.format(ssid, properties["Name"]) )
    if ssid == properties["Name"]:
      stop_hostapd()
      identifier = path[path.rfind("/") + 1:]
      path = '/net/connman/service/' + identifier
      service = dbus.Interface(bus.get_object("net.connman", path),
                                              "net.connman.Service")
      #except dbus.DBusException, error:
      #  print '{}: {}'.format(error._dbus_error_name, error.message)

      service_info = '''
[service_homewifi]
Type=wifi
Name={}
Passphrase={}
MAC={}
'''.format(ssid, password, wlan0_mac)
      syslog( service_info )
      service_path = os.path.join(os.sep,'var','lib','connman')
      with open( os.path.join(service_path, 'homewifi.config'), 'w' ) as f:
        f.write( service_info )
      time.sleep(2)
      syslog( 'Connect to ssid: {}'.format( ssid ) )
      service.Connect(timeout=60000)
      service.SetProperty("AutoConnect", True )
      for i in range(60):
        syslog( service.properties["State"] )
        if service.properties["State"] == "online":
          break
        time.sleep(5)
      #Fails print 'Favorite <- True'
      #service.SetProperty("Favorite", True )
      return True

def start_ap():
  syslog( 'Start ap script' )
  subprocess.check_output('/sbin/start_ap.sh')

def remove_previus_wifi():
  subprocess.check_output(["connmanctl", "config", service_name, "--remove"])

def main(ssid, password):
  connection_is = False
  try:
    connection_is =  connect_new_wifi(ssid, password)
  except dbus.DBusException, error:
    while ('net.connman.Error.InProgress' == error._dbus_error_name and 'In progress' == error.message ):
      syslog('Wireless connection In progress, waiting for the connection to establish')
      time.sleep(1)
      counter+=1
      if counter >60:
        break
    syslog( 'Failed to connect to new wifi due to {}: {}'.format(error._dbus_error_name, error.message) )
    start_ap()
  else:
    if connection_is:
      start_ap()
  return connection_is

def run():
  parser = argparse.ArgumentParser()
  parser.add_argument("name")
  parser.add_argument("password")
  args = parser.parse_args()
  main( args.name, args.password )

if __name__ == '__main__':
  run()
