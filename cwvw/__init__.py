#!/usr/bin/python
from flask import Flask, render_template, request, flash
from forms import SetupForm
import start_client
from syslog import syslog
import requests
import subprocess
import json
import os.path
import uuid

app = Flask(__name__)
app.secret_key = str(uuid.uuid4())
#start_client.start_ap()
debug_flag = False
wireless_config_file = '/var/lib/connman/homewifi.config'

@app.route('/', methods=['GET', 'POST'])
def wireless_setup():
  global connected
  form = SetupForm()
  if request.method == 'GET':
    saved_ssid = load_saved_ssid()
    if saved_ssid :
      form.ssid.data = saved_ssid
  elif request.method == 'POST':
    if form.validate() == False:
      flash('Error with the data')
    else:
      syslog( 'call start_client ssid: {} password: {}'.format( form.ssid.data, form.password.data ) )
      connected =  start_client.main( form.ssid.data, form.password.data )
      syslog('start_clinet returned connected={}'.format(connected))
      if not connected:
        flash('Failed to connect to wifi')
  return render_template('setup.html', form=form, local_ip=get_local_ip() )

@app.route('/connected', methods=['GET'])
def connected():
  if connected:
    return 'OK'
  return 'NO'

@app.route('/ssid_list', methods=['GET'])
def get_ssid_list():
  is_wireless = False
  for line in subprocess.check_output(['connmanctl', 'technologies']).split('\n'):
    if '/net/connman/technology/wifi' in line:
      is_wireless = True
      break
  if not is_wireless:
    return None
  ssid_list = list()
  subprocess.call(['connmanctl', 'scan', 'wifi'])
  for line in subprocess.check_output(['connmanctl', 'services']).split('\n'):
    splitted_line = line.split()
    if len(splitted_line) >1:
      name, service = splitted_line[-2], splitted_line[-1]
      if service.startswith('wifi'):
        syslog('Found ssid: {}'.format(name) )
        ssid_list.append( name )
    else:
      syslog('Found Hidden hotspot')
  return json.dumps(ssid_list)

def get_local_ip():
  try:
    for line in subprocess.check_output(['ip', 'addr', 'show', 'wlan0']).split('\n'):
      l=line.split()
      if len(l) >0  and l[0] == 'inet':
        local_ip = l[1].split('/')[0]
        url = 'http://{}'.format(local_ip)
        return url
  except:
    pass
  return None

def load_saved_ssid():
  if os.path.isfile(wireless_config_file):
    with open(wireless_config_file) as f:
      for line in f.readlines():
        if line.startswith('Name='):
          saved_ssid = line.split('=')[-1].strip()
          syslog('saved ssid: {}'.format(saved_ssid))
          return saved_ssid
  return None


if __name__ == '__main__':
  connected = False
  debug_flag = True
  app.run(host='0.0.0.0', port=80, debug=True)

