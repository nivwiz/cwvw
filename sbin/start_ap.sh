#!/bin/bash
set -x
[ $EUID -ne 0 ] && echo "This script must be run as root" && exit 1

ap_dev='wlan0_ap'
client_dev='wlan0'
password="MyWiFiPassword"
#sub='192.168.1'

wait_for_wlan() {
  for ((i=0 ; i<1000 ; i++)) ; do
    ip link show ${client_dev} && break
    sleep 10
  done
}
wait_for_wlan

allow_only_atheros() {
  man=$(</sys/bus/usb/devices/1-1/manufacturer)
  [ "$man" != "ATHEROS" ] && logger "usb 1-1 manufacturer is $man not ATHEROS, ending start_ap.sh" && exit 0
}
allow_only_atheros

[ -d "/sys/class/net/${client_dev}" ] && wlan_mac=$(iw ${client_dev} info |grep addr |cut -d' ' -f2 |tr -d ':-')
ssid="cwvw_${wlan_mac}"

stop_related_services() {
  #systemctl is-active dnsmasq_cwvw.service && systemctl stop dnsmasq_cwvw.service
  systemctl is-active dnsmasq.service && systemctl stop dnsmasq.service
  [ -d "/sys/class/net/tether" ] && connmanctl tether wifi off
  #systemctl is-active cwvw.service && systemctl stop cwvw.service
  #systemctl is-active cwvw.socket && systemctl stop cwvw.socket
  systemctl stop hostapd
}
stop_related_services

get_channel_used() {
  for (( i=1; i < 5; i++))
    do
      channel_used=$(iw ${client_dev} info |awk '/channel/ {print $2}')
      [ "x$channel_used" != "x" ] && break
      sleep 2
      echo 'Retry query for which chanel the device is using'
  done
  [ "x$channel_used" == "x" ]&& channel_used='6'
}
[ -d "/sys/class/net/${client_dev}" ] && get_channel_used

update_hostapd_config() {
  [ -n "$channel_used" ] && sed -i "s/^channel=.*$/channel=${channel_used}/" /etc/hostapd/hostapd.conf
  sed -i "s/^interface=.*$/interface=${ap_dev}/" /etc/hostapd/hostapd.conf
  sed -i "s/^ssid=.*$/ssid=${ssid}/" /etc/hostapd/hostapd.conf
  sed -i "s/^wpa_passphrase=.*$/wpa_passphrase=${password}/" /etc/hostapd/hostapd.conf
}
update_hostapd_config

fix_soft_block() {
  rfkill_id=$(rfkill list | awk '/phy0: Wireless LAN/{id=$1} /Soft blocked:/{state=$NF} END {if (state == "yes") {split(id,a,":"); print a[1]}} ' )
  [ "x$rfkill_id" != "x" ] && rfkill unblock $rfkill_id
}
fix_soft_block

create_ap_device() {
  if [ ! -d "/sys/class/net/${ap_dev}" ] ; then
    cat <<EOF >/etc/udev/rules.d/65-cwvw.rules
  # virtual access point
  SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="wlan*", NAME="${ap_dev}"
EOF
  udevadm control --reload
  phy=$(iw phy |awk '/Wiphy/ {print $2}')
  iw phy ${phy} interface add ${ap_dev} type __ap
  ip link set dev ${ap_dev} address 12:34:56:78:ab:cd
  #ip link set up ${ap_dev}
  fi
  [ -f "/etc/udev/rules.d/65-cwvw.rules" ] && /bin/rm /etc/udev/rules.d/65-cwvw.rules && udevadm control --reload
  if [ ! -d "/sys/class/net/${ap_dev}" ] ; then
    logger "start_ap.sh failed to find the interface ${ap_dev}"
    exit 1
  fi
}
create_ap_device

create_connman_ap_conf() {
    cat <<EOF >'/var/lib/connman/access_point'
[access_point]
Name=access_point
MAC=12:34:56:78:ab:cd
Favorite=true
AutoConnect=true
IPv4.method=manual
IPv6.method=auto
IPv6.privacy=disabled
IPv4.netmask_prefixlen=28
IPv4.local_address=${ap_ip}
EOF
}

set_access_point_ip() {
  ap_ip=$(ip addr show ${ap_dev} |awk '/inet / {split($2,a,"/"); print a[1]}')
  if [ "x$ap_ip" == "x" ] ; then
      #here we should check this subnet is not in use.
      #wlan_subnet=$(ip addr show ${client_dev} |awk 'BEGIN{OFS="."} /inet / { split($2,a,"/"); split(a[1],b,".") ; print b[1],b[2],b[3]}')
      #eth_subnet=$(ip addr show eth0 |awk 'BEGIN{OFS="."} /inet / { split($2,a,"/"); split(a[1],b,".") ; print b[1],b[2],b[3]}')
      system_used_subnets=$(ip ro |awk '!/default/ {gsub("/", "", $1) ;print $1}')
    for ((index=1; index <=255 ; index++ )) ; do
	sub="192.168.$index"
	for line in ${system_used_subnets} ; do
            [[ "$line" =~ ^$sub ]] && break
            #[[ "$sub" != "${wlan_subnet}" ]] && [[ "$sub" != "${eth_subnet}" ]] && break
	done
	[[ "$line" =~ ^$sub ]] && continue
	break
    done
    ip addr add ${sub}.2/28 broadcast ${sub}.255 dev ${ap_dev}
    create_connman_ap_conf
  fi
}
set_access_point_ip

sleep 1

systemctl restart hostapd

allow_dhcp_only_to_ap_dev() {
  echo 'Block dhcp requests using iptables'
  /sbin/iptables -A INPUT -p udp -m udp --sport 67 --dport 68 -j DROP
  /sbin/iptables -A INPUT -i ${ap_dev} -p udp -m udp --sport 67 --dport 68 -j ACCEPT
}
allow_dhcp_only_to_ap_dev

run_dnsmasq() {
  sed -i "s/^interface=.*$/interface=${ap_dev}/" /etc/dnsmasq_wlan0_ap.conf
  ap_ip=$(ip addr show ${ap_dev} |awk '/inet / {split($2,a,"/") ;print a[1] }')
  ap_sub=${ap_ip%.*}
  sed -i "s/^listen-address=.*$/listen-address=${ap_ip}/" /etc/dnsmasq_wlan0_ap.conf
  sed -i "s/^address=.*$/address=\/#\/${ap_ip}/" /etc/dnsmasq_wlan0_ap.conf
  #[ -z "$ap_sub" ] && sub=$(ip addr show ${ap_dev} |awk 'BEGIN{OFS="."} /inet / { split($2,a,"/"); split(a[1],b,".") ; print b[1],b[2],b[3]}')
  sed -i "s/^dhcp-range=.*$/dhcp-range=${ap_sub}.3,${ap_sub}.7,1h/" /etc/dnsmasq_wlan0_ap.conf
  [ -d '/run/dnsmasq/' ] || mkdir /run/dnsmasq/
  systemctl restart dnsmasq_cwvw
}
run_dnsmasq

#systemctl is-active cwvw.socket || systemctl is-active cwvw.service || systemctl restart cwvw.socket
systemctl is-active cwvw.service || systemctl restart cwvw.service
